
# IPTABLES Command 
IPTABLES="/usr/sbin/iptables"
IPTSave="/usr/sbin/iptables-save > /etc/iptables/rules.v4"
# Flush: apagar todas as regras anteriores
$IPTABLES -F
$IPTABLES -X 
$IPTABLES -Z 

$IPTABLES -t nat -F 
$IPTABLES -t nat -X 
$IPTABLES -t nat -Z
$IPTABLES -t mangle -F
$IPTABLES -t mangle -X 
$IPTABLES -t mangle -Z
$IPTABLES -t raw -F 
$IPTABLES -t raw -X 
$IPTABLES -t raw -Z

# Negar todo o tráfico 
$IPTABLES -P INPUT ACCEPT
$IPTABLES -P FORWARD ACCEPT
$IPTABLES -P OUTPUT ACCEPT
